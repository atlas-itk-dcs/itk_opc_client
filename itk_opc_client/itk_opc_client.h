#ifndef ITK_OPC_CLIENT
#define ITK_OPC_CLIENT

#include <open62541/open62541.h>
#include <itk_opc_client/itk_opc_nodeid.h>
#include <itk_opc_client/itk_opc_variant.h>
#include <string>
#include <vector>
#include <sstream>

class itk_opc_client{

 public:

  /**
   * Create a new client given a connection string
   **/
  itk_opc_client(std::string connstr);

  /**
   * Delete the client. Disconnect if still connected
   **/
  ~itk_opc_client();

  /**
   * Connect to the opc server
   * @return true if connected, else return false
   **/
  bool Connect();

  /**
   * Disconnect from the server
   * @return true if disconnected properly, else return false
   **/
  bool Disconnect();

  /**
   * Find the nodes available in the server. If no parameter given start from the root objects folder.
   * @param nodeId nodeId where to start looking for nodes available
   **/
  void FindNodes(itk_opc_nodeid nodeId=itk_opc_nodeid::numeric(0, UA_NS0ID_OBJECTSFOLDER));

  /**
   * Get the list of nodes. FindNodes has to be called first
   * @return vector of nodes 
   **/
  std::vector<itk_opc_nodeid> GetNodes();

  /**
   * Get a value from the server by specifying its nodeid
   * @param nodeId the node to read  
   * @return variant with the value of the node
   **/
  itk_opc_variant GetValue(itk_opc_nodeid nodeId);

  /**
   * Set a value of a nodeid in the server
   * @param nodeId the node to write
   * @param value the value to write
   * @return true if write was successful, return false otherwise
   **/
  bool SetValue(itk_opc_nodeid nodeId, itk_opc_variant value);
  
private:
  UA_Client * m_client;
  std::string m_connstr;
  bool m_verbose;
  std::vector<itk_opc_nodeid> m_nodes;
};

#endif

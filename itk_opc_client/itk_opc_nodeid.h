#ifndef ITK_OPC_NODEID
#define ITK_OPC_NODEID

#include <open62541/open62541.h>

#include <string>
#include <sstream>
#include <cstdint>
#include <vector>

class itk_opc_nodeid{

 public:
  
  /**
   * Create an empty object
   **/
  itk_opc_nodeid();

  /**
   * Delete the object
   **/
  ~itk_opc_nodeid();
  
  /**
   * Convert into a UA_NodeId
   * @return UA_NodeId
   **/
  UA_NodeId toNodeId();

  /**
   * Parse from a UA_Node
   * @param nodeId the UA_NodeId
   **/
  void fromNodeId(UA_NodeId * nodeId);

  /**
   * Create an object of UA_NODEIDTYPE_NUMERIC
   * @param namespaceIndex namespace index of the nodeid
   * @param numeric uint32_t value
   **/
  static itk_opc_nodeid numeric(uint32_t namespaceIndex, uint32_t numeric);

  /**
   * Create an object of UA_NODEIDTYPE_STRING
   * @param namespaceIndex namespace index of the nodeid
   * @param identifier string value
   **/
  static itk_opc_nodeid string(uint32_t namespaceIndex, std::string identifier);

  /**
   * Namespace index
   **/
  uint32_t      namespaceIndex;

  /**
   * Identifier type
   **/
  UA_NodeIdType identifierType;

  /**
   * Identifier content
   **/
  struct itk_opc_identifier{
    uint32_t     numeric;
    std::string  string;
    UA_Guid      guid;
    std::string  byteString;
  } identifier;


};

std::ostream &operator<<(std::ostream &os, itk_opc_nodeid const &nodeId);
 
#endif


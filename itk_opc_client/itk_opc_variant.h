#ifndef ITK_OPC_VARIANT
#define ITK_OPC_VARIANT

#include <open62541/open62541.h>
#include <string>
#include <vector>

class itk_opc_variant{

 public:

  /**
   * Create an empty object
   **/
  itk_opc_variant();

  /**
   * Release the internal memory
   **/
  ~itk_opc_variant();

  /**
   * Get the internal UA_Variant
   * @return internal UA_Variant
   **/
  UA_Variant * GetData();

  /**
   * Type cast to unsigned integer or return zero
   * @return unsigned integer
   **/
  uint32_t toUInt32();

  /**
   * Type cast to string or return an empty string
   * @return string 
   **/
  std::string toString();

  /**
   * Set the type of the variant to UA_TYPES_UINT32 and set the value
   * @param value unsigned integer
   **/
  void setUInt32(uint32_t value);

  /**
   * Set the type of the variant to UA_TYPES_STRING and set the value
   * @param value string
   **/
  void setString(std::string value);
  
  /**
   * Create a new object of type UA_TYPES_UINT32
   * @param value unsigned integer
   * @return variant object of type UA_TYPES_UINT32
   **/
  static itk_opc_variant UInt32(uint32_t value);
  
  /**
   * Create a new object of type UA_TYPES_STRING
   * @param value string
   * @return variant object of type UA_TYPES_STRING
   **/
  static itk_opc_variant String(std::string value);
  
 private:
  UA_Variant * m_data;

};

#endif

#!/bin/bash

# the purpose of this script is to make the process of deploying open62541 in open62541-compat
# automated, reproducible

# this script was created to be used only by quasar-developers team.

TAG=v1.2.2

# prepare the dir structure
SRC_PATH=$(readlink -f $(dirname ${BASH_SOURCE[0]}))
mkdir -p ${SRC_PATH}/open62541

# prepare (i.e. amalgamate... open62541)
WD=`pwd`
if [ -d tmp ]; then
  rm -frv tmp
fi
mkdir tmp && cd tmp
git clone https://github.com/open62541/open62541.git --depth=1 -b $TAG || exit
cd open62541
mkdir build && cd build
cmake -DUA_ENABLE_AMALGAMATION=ON -DUA_ENABLE_METHODCALLS=ON -DUA_LOGLEVEL=100 ../ || exit
make || exit
cd $WD

# deploy files
cp tmp/open62541/build/open62541.h ${SRC_PATH}/open62541
cp tmp/open62541/build/open62541.c ${SRC_PATH}/open62541

# clean-up
rm -fr tmp

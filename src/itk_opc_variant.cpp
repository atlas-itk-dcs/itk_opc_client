#include <itk_opc_client/itk_opc_variant.h>

using namespace std;

itk_opc_variant::itk_opc_variant(){
  m_data = UA_Variant_new();
}

itk_opc_variant::~itk_opc_variant(){
  UA_Variant_delete(m_data);
}

UA_Variant * itk_opc_variant::GetData(){
  return m_data;
}

void itk_opc_variant::setUInt32(uint32_t value){
  UA_Variant_setScalarCopy(m_data,(void*)&value,&UA_TYPES[UA_TYPES_UINT32]);
}

void itk_opc_variant::setString(string value){
  UA_Variant_setScalarCopy(m_data,(void*)&value,&UA_TYPES[UA_TYPES_STRING]);
}

uint32_t itk_opc_variant::toUInt32(){
  return *(uint32_t*)m_data->data;
}

string itk_opc_variant::toString(){
  return string((char*)m_data->data,m_data->arrayLength);
}

itk_opc_variant itk_opc_variant::UInt32(uint32_t value){
  itk_opc_variant variant;
  variant.setUInt32(value);
  return variant;
}

itk_opc_variant itk_opc_variant::String(string value){
  itk_opc_variant variant;
  variant.setString(value);
  return variant;
}

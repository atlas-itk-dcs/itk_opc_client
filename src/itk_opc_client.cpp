#include <itk_opc_client/itk_opc_client.h>
#include <iostream>

using namespace std;

itk_opc_client::itk_opc_client(string connstr){
  m_connstr = connstr;
  m_verbose = true;
  m_client = 0;
}

itk_opc_client::~itk_opc_client(){
  if(m_client){
    UA_Client_delete(m_client);
    m_client=0;
  }
}

bool itk_opc_client::Connect(){
  m_client = UA_Client_new();
  UA_ClientConfig_setDefault(UA_Client_getConfig(m_client));
  UA_StatusCode ret = UA_Client_connect(m_client, m_connstr.c_str());
  if(ret != UA_STATUSCODE_GOOD) {
    cout << "Could not connect to server: " << m_connstr << endl;
    UA_Client_delete(m_client);
    return false;
  }
  return true;
}

bool itk_opc_client::Disconnect(){
  UA_StatusCode ret = UA_Client_disconnect(m_client);
  if(ret != UA_STATUSCODE_GOOD) {
    cout << "Error while disconnecting from server: " << m_connstr << endl;
    UA_Client_delete(m_client);
    return false;
  }
  UA_Client_delete(m_client);
  m_client=0;
  return true;
}

itk_opc_variant itk_opc_client::GetValue(itk_opc_nodeid nodeId){
  itk_opc_variant value;
  UA_StatusCode ret = UA_Client_readValueAttribute(m_client, nodeId.toNodeId(), value.GetData());
  return value;  
}

bool itk_opc_client::SetValue(itk_opc_nodeid nodeId, itk_opc_variant value){
  UA_StatusCode ret = UA_Client_writeValueAttribute(m_client, nodeId.toNodeId(), value.GetData());
  if(ret == UA_STATUSCODE_GOOD) return true;
  return false;  
}

vector<itk_opc_nodeid> itk_opc_client::GetNodes(){
  return m_nodes;
}

void itk_opc_client::FindNodes(itk_opc_nodeid nodeId){
  if(m_verbose) {
    cout << "GetChildren: " << nodeId << endl;
  }
  UA_BrowseRequest bReq;
  UA_BrowseRequest_init(&bReq);
  bReq.requestedMaxReferencesPerNode = 0;
  bReq.nodesToBrowse = UA_BrowseDescription_new();
  bReq.nodesToBrowseSize = 1;
  bReq.nodesToBrowse[0].nodeId = nodeId.toNodeId();
  bReq.nodesToBrowse[0].resultMask = UA_BROWSERESULTMASK_ALL;
  UA_BrowseResponse bResp = UA_Client_Service_browse(m_client, bReq);
  for(size_t i = 0; i < bResp.resultsSize; ++i) {
    for(size_t j = 0; j < bResp.results[i].referencesSize; ++j) {
      UA_ReferenceDescription *ref = &(bResp.results[i].references[j]);
      if(m_verbose){
	cout << "browseName: " << string((char*)ref->browseName.name.data,(int)ref->browseName.name.length) << ", "
	     << "displayName: " << string((char*)ref->displayName.text.data,(int)ref->displayName.text.length) << ", "
	     << "nodeClass: " << ref->nodeClass << ", "
	     << "isForward: " << ref->isForward << ", "
	     << "nodeId.namespaceIndex: " << ref->nodeId.nodeId.namespaceIndex << ", "
	     << "nodeId.identifierType: " << ref->nodeId.nodeId.identifierType << ", ";
	if(ref->nodeId.nodeId.identifierType==UA_NODEIDTYPE_STRING){
	  cout << "nodeId: " << string((char*)ref->nodeId.nodeId.identifier.string.data,(int)ref->nodeId.nodeId.identifier.string.length) << ", ";
	}else if(ref->nodeId.nodeId.identifierType==UA_NODEIDTYPE_NUMERIC){
	  cout << "nodeId: " << ref->nodeId.nodeId.identifier.numeric << ", ";
	}
	cout << endl;
      }
      if(ref->nodeId.nodeId.namespaceIndex==2){
	itk_opc_nodeid nnodeid;
	nnodeid.fromNodeId(&ref->nodeId.nodeId);
	m_nodes.push_back(nnodeid);
	FindNodes(nnodeid);
      }
    }
  }
  UA_BrowseRequest_clear(&bReq);
  UA_BrowseResponse_clear(&bResp);
  if(m_verbose) cout << "End GetChildren" << endl;
}

#include <itk_opc_client/itk_opc_client.h>

#include <iostream>
#include <cstdint>

using namespace std;

int main(int argc, char** argv){

  if (argc<2){
    cout << "Usage: itk_opc_client connstr" << endl
	 << " connstr  opc.tcp://host:port" << endl;
    return 1;
  }
  
  itk_opc_client *opc=new itk_opc_client(argv[1]);
  opc->Connect();
  opc->FindNodes();
  cout << "Found nodes " << endl;
  for(auto nodeId: opc->GetNodes()){
    cout << nodeId << endl;
  }
  itk_opc_nodeid nodeId = itk_opc_nodeid::string(2,"bus1.elmb1.TPDO3.ch1.value");
  cout << "addr: " << nodeId << endl;
  itk_opc_variant val = opc->GetValue(nodeId);
  cout << "val:  " << val.toUInt32() << endl;

  cout << "test read:  " << opc->GetValue(itk_opc_nodeid::string(2,"bus1.elmb1.TPDO3.ch0.value")).toUInt32() << endl;

  cout << "test write: " << opc->SetValue(itk_opc_nodeid::string(2,"bus1.elmb1.TPDO3.ch0.value"),
					  itk_opc_variant::UInt32(3222)) << endl;

  opc->Disconnect();
  delete opc;
  return 0;
}

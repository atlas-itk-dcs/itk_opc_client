#include <itk_opc_client/itk_opc_nodeid.h>

using namespace std;

itk_opc_nodeid::itk_opc_nodeid(){
  namespaceIndex=0;
  identifierType=UA_NODEIDTYPE_STRING;
}

itk_opc_nodeid::~itk_opc_nodeid(){}

UA_NodeId itk_opc_nodeid::toNodeId(){
  if     (identifierType==UA_NODEIDTYPE_STRING)    {return UA_NODEID_STRING_ALLOC(namespaceIndex,identifier.string.c_str());}
  else if(identifierType==UA_NODEIDTYPE_NUMERIC)   {return UA_NODEID_NUMERIC(namespaceIndex,identifier.numeric);}
  else if(identifierType==UA_NODEIDTYPE_GUID)      {return UA_NODEID_GUID(namespaceIndex,identifier.guid);}
  else if(identifierType==UA_NODEIDTYPE_BYTESTRING){return UA_NODEID_BYTESTRING_ALLOC(namespaceIndex,identifier.byteString.c_str());}
  return UA_NODEID_NUMERIC(0,0);
}

itk_opc_nodeid itk_opc_nodeid::numeric(uint32_t namespaceIndex, uint32_t numeric){
  itk_opc_nodeid ret;
  ret.namespaceIndex=namespaceIndex;
  ret.identifierType=UA_NODEIDTYPE_NUMERIC;
  ret.identifier.numeric=numeric;
  return ret;
}

itk_opc_nodeid itk_opc_nodeid::string(uint32_t namespaceIndex, std::string identifier){
  itk_opc_nodeid ret;
  ret.namespaceIndex=namespaceIndex;
  ret.identifierType=UA_NODEIDTYPE_STRING;
  ret.identifier.string=identifier;
  return ret;
}

void itk_opc_nodeid::fromNodeId(UA_NodeId * nodeId){
  namespaceIndex=nodeId->namespaceIndex;
  identifierType=nodeId->identifierType;
  if     (nodeId->identifierType==UA_NODEIDTYPE_STRING)    {identifier.string=std::string((char*)nodeId->identifier.string.data,(int)nodeId->identifier.string.length);}
  else if(nodeId->identifierType==UA_NODEIDTYPE_NUMERIC)   {identifier.numeric=nodeId->identifier.numeric;}
  else if(nodeId->identifierType==UA_NODEIDTYPE_GUID)      {identifier.guid=nodeId->identifier.guid;}
  else if(nodeId->identifierType==UA_NODEIDTYPE_BYTESTRING){identifier.byteString=std::string((char*)nodeId->identifier.byteString.data,(int)nodeId->identifier.byteString.length);}
}

std::ostream &operator<<(std::ostream &os, itk_opc_nodeid const &nodeId){ 
  os << "(ns=" << nodeId.namespaceIndex;
  if     (nodeId.identifierType==UA_NODEIDTYPE_STRING) { os << ", s='" << nodeId.identifier.string << "'";}
  else if(nodeId.identifierType==UA_NODEIDTYPE_NUMERIC){ os << ", n=" << nodeId.identifier.numeric;}
  else if(nodeId.identifierType==UA_NODEIDTYPE_GUID){ os << ", g=" << nodeId.identifier.guid.data1;}
  else if(nodeId.identifierType==UA_NODEIDTYPE_BYTESTRING){ os << ", bs='" << nodeId.identifier.byteString << "'";}
  return os << ")";
}
